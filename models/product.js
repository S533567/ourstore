/**
 * @author Mushafiq
 */
const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

  _Id: { type: Number, required: true },
  productName: {
    type: String,
    required: true,
    default: 'mush',
  }, 
  productDescription: {
    type: String,
    required: true,
    default: 'abcd',
    min: 1,
    max: 20,
  }, 
  price: {
    type: Number,
    required: true,
    default: 000
  },
  size:{
    type: String,
    required: true,
    default: 00
  }
})
module.exports = mongoose.model('Product', ProductSchema)