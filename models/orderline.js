/**
 * @author surya
 */

const mongoose = require('mongoose')
const orderLineSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  orderID: {
    type: Number,
    required: true,
    min: 1,
  },
  lineNumber: {
    type: Number,
    required: true
  },
  productKey: {
  type:String,
  required:true,
  default:'aaa'
  },
  quantity: {
    type: Number,
    required: true, 
    default: 1
  }
})
module.exports = mongoose.model('orderLine', orderLineSchema)